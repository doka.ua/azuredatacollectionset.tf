# art by doka@funlab.cc

variable "resource_group_name" {
  type = string
}

variable "data_collection_endpoint_name" {
  type = string
}

variable "mappings" {
  type = map(
    object(
      {
        resource_ids               = list(string)
        data_collection_rule_names = list(string)
      }
    )
  )
}

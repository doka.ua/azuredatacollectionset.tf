resource_group_name = "myResourceGroup"
data_collection_endpoint_name = "vmset-dce-test"

mappings = {
    map1 = {
        resource_ids = [
            "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/my-vm-01",
            "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/my-vm-02"
        ]
        data_collection_rule_names = [
            "vmset-dcr-blue",
            "vmset-dcr-red"
        ]
    },
    map2 = {
        resource_ids = [
            "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/my-vm-01",
            "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/my-vm-02",
            "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.Compute/virtualMachines/my-vm-03",
        ]
        data_collection_rule_names = [
            "vmset-dcr-vmperf"
        ]
    }
}

# art by doka@funlab.cc

data "azurerm_resources" "dcr" {
  type = "Microsoft.Insights/dataCollectionRules"
  resource_group_name = var.resource_group_name
}

data "azurerm_resources" "dce" {
  type = "Microsoft.Insights/dataCollectionEndpoints"
  resource_group_name = var.resource_group_name
}

locals {
    # Find DCE id by name (here we need 'name-ctl' DCE, defined during the previous step)
    dce_name_ctl = format("%s-ctl", var.data_collection_endpoint_name)
    dce_ids      = { for dce in data.azurerm_resources.dce.resources : dce.name => dce.id if dce.name == local.dce_name_ctl }
    dce_id_ctl   = lookup(local.dce_ids, local.dce_name_ctl, "")
    # Prepare list of all DCR IDs, existing in the RG
    dcr_ids      = { for dcr in data.azurerm_resources.dcr.resources : dcr.name => dcr.id }
    
    r2dcr        = distinct(flatten([
        for mx in var.mappings : [
            for rid in mx.resource_ids : [
                for dcr_name in mx.data_collection_rule_names : {
                    dcr_id      = local.dcr_ids[dcr_name]
                    resource_id = rid
                } if length(lookup(local.dcr_ids, dcr_name, "")) > 0
            ]
        ]
    ]))

    r2dce        = distinct(flatten([
        for mx in var.mappings : [
            for rid in mx.resource_ids : {
                dce_id_ctl  = local.dce_id_ctl
                resource_id = rid
            }
        ]
    ]))
}

resource "azurerm_monitor_data_collection_rule_association" "dcr" {
  # Create uniq index assoc list, e.g. azurerm_monitor_data_collection_rule_association.dcr["vm-01--dcr-vmperf"]
  for_each                = { for e in local.r2dcr : format("%s--%s", split("/", e.resource_id)[8], split("/", e.dcr_id)[8]) => e }
  # The name of this dcr is a combination of the resource group name and the name of the DCR.
  name                    = join ( "-", [ split ( "/" , each.value.dcr_id )[4], split ( "/" , each.value.dcr_id )[8] ] )
  target_resource_id      = each.value.resource_id
  data_collection_rule_id = each.value.dcr_id
}

resource "azurerm_monitor_data_collection_rule_association" "ctl" {
  for_each                    = { for e in local.r2dce : split("/", e.resource_id)[8] => e if e.dce_id_ctl != ""}
  target_resource_id          = each.value.resource_id
  data_collection_endpoint_id = each.value.dce_id_ctl
}

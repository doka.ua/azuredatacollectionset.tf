terraform {
  required_providers {
    azurerm = {
        source  = "hashicorp/azurerm"
        #version = "=3.64"
    }
    azuread = {
        #version         = "=1.6.0"
    }
  }
  #required_version = "~> 0.12.20"
}

provider azurerm {
  features {}
}

provider azuread {
  features {}
}

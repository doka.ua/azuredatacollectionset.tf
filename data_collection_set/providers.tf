terraform {
  required_providers {
    azurerm = {
        source  = "hashicorp/azurerm"
        #version = "> 3.60"
    }
    azuread = {
        source  = "hashicorp/azuread"
        #version = "> 1.6.0"
    }
    azapi = {
      source  = "Azure/azapi"
      #version = "> 1.8.0"
    }
  }
  required_version = ">= 1.7.5"
}

provider azurerm {
  features {}
}

provider azuread {
  features {}
}

# art by doka@funlab.cc

# unlike data.azurerm_log_analytics_workspace.this, this request is subscription wide
data "azurerm_resources" "law" {
  type = "Microsoft.OperationalInsights/workspaces"
}

data "azurerm_resource_group" "this" {
  name = var.resource_group_name
}

locals {
    location                         = length(var.location) > 0 ? var.location : data.azurerm_resource_group.this.location
    # Find LAW of Interest and extract all required data
    log_analytics_workspace          = {
      for law in data.azurerm_resources.law.resources : "loi" => law if (
            law.id   == var.log_analytics_workspace_name_or_id ||
          ( law.name == var.log_analytics_workspace_name_or_id && law.resource_group_name == var.resource_group_name ))
    }
    log_analytics_workspace_name     = length(local.log_analytics_workspace) > 0 ? local.log_analytics_workspace["loi"].name : ""
    log_analytics_workspace_id       = length(local.log_analytics_workspace) > 0 ? local.log_analytics_workspace["loi"].id : ""
    log_analytics_workspace_location = length(local.log_analytics_workspace) > 0 ? local.log_analytics_workspace["loi"].location : ""

    dce_count                        = length(var.vm_insights_data_collection_rule_name) > 0 || length(var.custom_text_log_rules) > 0 ? 1 : 0
    # Data DCE
    data_collection_endpoint_id      = local.dce_count > 0 ? azurerm_monitor_data_collection_endpoint.dce[0].id : ""
    # Configuration DCE (specified DCE name will be prefixed with '-ctl')
    data_collection_endpoint_id_ctl  = local.dce_count > 0 ? azurerm_monitor_data_collection_endpoint.ctl[0].id : ""
}

###
### Data collection rule for VM/VMSS Insights
###
resource "azurerm_monitor_data_collection_rule" "insights" {
    count               = length(var.vm_insights_data_collection_rule_name) == 0 ? 0 : 1

    name                = var.vm_insights_data_collection_rule_name
    resource_group_name = var.resource_group_name
    location            = local.log_analytics_workspace_location
    tags                = var.data_collection_tags

    destinations {
        log_analytics {
            workspace_resource_id = local.log_analytics_workspace_id
            name                  = local.log_analytics_workspace_name
        }
    }

    data_flow {
        streams      = ["Microsoft-InsightsMetrics"]
        destinations = [local.log_analytics_workspace_name]
    }

    data_sources {
        performance_counter {
            streams                       = ["Microsoft-InsightsMetrics"]
            sampling_frequency_in_seconds = 60
            counter_specifiers            = ["\\VmInsights\\DetailedMetrics"]
            name                          = "VMInsightsPerfCounters"
        }
    }
}

### Data collection endpoints for this set of DCRs
### https://learn.microsoft.com/en-us/azure/azure-monitor/essentials/data-collection-endpoint-overview?tabs=portal#components-of-a-data-collection-endpoint
###
### Configuration endpoint - need to be in the same region as connected resource
resource "azurerm_monitor_data_collection_endpoint" "ctl" {
    count                         = local.dce_count
    name                          = format("%s-ctl", var.data_collection_endpoint_name)
    resource_group_name           = var.resource_group_name
    location                      = local.location
    kind                          = var.operating_system_kind
    public_network_access_enabled = var.data_collection_endpoint_public_access
    tags                          = var.data_collection_tags
}

###
### Logs Injection endpoint - need to be in the same region with LA workspace
resource "azurerm_monitor_data_collection_endpoint" "dce" {
    count                         = local.dce_count
    name                          = var.data_collection_endpoint_name
    resource_group_name           = var.resource_group_name
    location                      = local.log_analytics_workspace_location
    kind                          = var.operating_system_kind
    public_network_access_enabled = var.data_collection_endpoint_public_access
    tags                          = var.data_collection_tags
}

### Custom tables and DCRs for custom text logs
###
### -- Custom tables
resource "azapi_resource" "data_collection_logs_table" {
  for_each  = {for each in var.custom_text_log_rules : each.custom_table_name => each if each.custom_table_create}
  name      = each.value.custom_table_name
  parent_id = local.log_analytics_workspace_id
  # https://learn.microsoft.com/en-us/azure/templates/microsoft.operationalinsights/allversions
  type      = "Microsoft.OperationalInsights/workspaces/tables@2022-10-01"
  body = jsonencode(
    {
      "properties" : {
        "schema" : {
          "name" : each.value.custom_table_name,
          "columns" : [
            {
              "name"        : "TimeGenerated",
              "type"        : "datetime",
              "description" : "The time at which the data was generated"
            },
            {
              "name"        : "RawData",
              "type"        : "string",
              "description" : "The log entry"
            }
          ]
        }
      }
    }
  )
}

### -- DCRs for custom text logs
resource "azurerm_monitor_data_collection_rule" "this" {
  for_each                    = var.custom_text_log_rules
  name                        = each.value.data_collection_rule_name
  resource_group_name         = var.resource_group_name
  location                    = local.log_analytics_workspace_location
  kind                        = var.operating_system_kind
  # It's a Logs Injection endpoint
  data_collection_endpoint_id = local.data_collection_endpoint_id
  description                 = each.value.data_collection_rule_description
  identity {
    type         = each.value.data_collection_rule_identity.type
    identity_ids = each.value.data_collection_rule_identity.identity_ids
  }
  tags                = var.data_collection_tags

  data_sources {
    log_file {
      name          = each.value.custom_table_name
      format        = "text"
      streams       = ["Custom-Text-${each.value.custom_table_name}"]
      file_patterns = each.value.data_collection_rule_file_patterns
      settings {
        text {
          record_start_timestamp_format = "ISO 8601"
        }
      }
    }
  }

  destinations {
    log_analytics {
      workspace_resource_id = local.log_analytics_workspace_id
      name                  = local.log_analytics_workspace_name
    }
  }

  data_flow {
    streams       = ["Custom-Text-${each.value.custom_table_name}"]
    destinations  = [local.log_analytics_workspace_name]
    output_stream = "Custom-${each.value.custom_table_name}"
    transform_kql = "source"
  }

  stream_declaration {
    stream_name = "Custom-Text-${each.value.custom_table_name}"
    column {
      name = "TimeGenerated"
      type = "datetime"
    }
    column {
      name = "RawData"
      type = "string"
    }
  }
  
  depends_on = [
    azapi_resource.data_collection_logs_table,
    azurerm_monitor_data_collection_endpoint.dce
  ]
}

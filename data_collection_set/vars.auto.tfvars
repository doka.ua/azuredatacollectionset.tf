resource_group_name = "myResourceGroup"

#- Optional, default to resource group's location
#location = "eastus"

#- Specify name if LAW resides in the same resource group or full ID (/subscription/...)
log_analytics_workspace_name_or_id = "/subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myResourceGroup/providers/Microsoft.OperationalInsights/workspaces/my-law-01"

vm_insights_data_collection_rule_name = "vmset-dcr-vmperf"

#- Whether access to DCEs enabled over public networks
#data_collection_endpoint_public_access = false

#- NOTE: two DCEs will be created: 'name' and 'name-ctl'
#- If public access to DCEs is disabled, BOTH DCEs need to be added
#- ... to the respective Azure Monitor Private Link Space (AMPLS)
data_collection_endpoint_name = "vmset-dce-logs"

#- Kind of OS on the group of VMs
#operating_system_kind = "Linux"

custom_text_log_rules = {
    red = {
        custom_table_name                  = "red_CL"
        #- If table is already created by another mean, set this to false
        #custom_table_create                = true
        data_collection_rule_name          = "vmset-dcr-red"
        data_collection_rule_file_patterns = ["/var/log/red.log"]
        #data_collection_rule_description   = ""
        #data_collection_rule_identity      = {
        #    type         = "SystemAssigned"
        #    identity_ids = []
        #}
    },
    blue = {
        custom_table_name                  = "blue_CL"
        data_collection_rule_name          = "vmset-dcr-blue"
        data_collection_rule_file_patterns = ["/var/log/blue.log"]
    }
}

data_collection_tags = {
    created_by = "doka@funlab.cc"
}

# art by doka@funlab.cc

variable "resource_group_name" {
  type        = string
  description = "Specifies the resource group for Data Collection Group"
}

variable "location" {
  type        = string
  description = "Specifies the location of connected resources (optional, default is resource group location)"
  default     = ""
}

variable "log_analytics_workspace_name_or_id" {
  type        = string
  description = "Specifies the id of the Log Analytics Workspace"
}

variable "vm_insights_data_collection_rule_name" {
  type        = string
  description = "Specifies the name of the Insights Data Collection Rule"
  default     = ""
}

variable "data_collection_endpoint_name" {
  type        = string
  description = "Specifies the name of the Data Connection Endpoint"
}

variable "data_collection_endpoint_public_access" {
  type        = bool
  description = "Defines whether DCEs can be accessed over public network, default is no"
  default     = false
}

variable "operating_system_kind" {
  type        = string
  description = "Specifies the kind of VM/VMSS Operarting System (optional, default is Linux)"
  default     = "Linux"
}

variable "custom_text_log_rules" {
  type = map(object({
      custom_table_name                  = string
      custom_table_create                = optional(bool, true)
      data_collection_rule_name          = string
      data_collection_rule_file_patterns = list(string)
      data_collection_rule_description   = optional(string, "")
      data_collection_rule_identity      = optional(object({
          type         = string
          identity_ids = list(string)
        }),
        {
          type = "SystemAssigned",
          identity_ids = []
        }
      )
  }))
}

variable "data_collection_tags" {
  type = map
}

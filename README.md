# Infrastructure for Azure Monitor Agent
Two Terraform sets:

- data_collection_set
- data_collection_set_association

which work together to create an infrastructure for gathering logs and Insights from the specified resources.

## What they do
### data_collection_set
Creates:
1. configuration and log injection Data Collection Endpoints
2. Data Collection Rule for Insights (VM Performance Metrics)
3. custom tables in Log Analytics Workspace
4. Data Collection Rule(s) for gathering and putting text logs into the respective custom table

### data_collection_set_association
Associates specified resources (VMs) with the specified Data Collection Rules / Endpoints, created during the previous step.

## What they do not do
These modules do not create Log Analytics Workspace, Azure Monitor Private Links Space (AMPLS) and do not add created DCEs to AMPLS.

## What is not covered
Gathering of syslogs. It can be done, though, by adding DCR for syslog file using existing code.

# Configuration
All configuration is stored in respective vars.auto.tfvar

Optional variables are commented, their default values are specified

# Author
Volodymyr Litovka, doka@funlab.cc
